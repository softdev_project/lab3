/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tanisorn.lab3;

import static com.tanisorn.lab3.Lab3.table;

/**
 *
 * @author uSeR
 */
class OXProgram {

    // Refactor
    static boolean checkWin(char[][] table, char currentPlayer) {
        if (checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkX1(table, currentPlayer) || checkX2(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(char[][] table, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] table, char currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(char[][] table, char currentPlayer, int row) {
        return (table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer);
    }

    private static boolean checkCol(char[][] table, char currentPlayer, int col) {
        return (table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer);
    }
    
    private static boolean checkX1(char[][] table, char currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }
    
    private static boolean checkX2(char[][] table, char currentPlayer) {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }
    

    static boolean checkDraw(char[][] table, char currentPlayer) {
       for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
}
