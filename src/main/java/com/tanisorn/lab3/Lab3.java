/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.tanisorn.lab3;

import java.util.Scanner;

/**
 *
 * @author uSeR
 */
public class Lab3 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWin()) {
                printTable();
                printWin();
                printAgain();
                break;
                
            }
            
            if (isDraw()) {
                printTable();
                printDraw();
                printAgain();
                break;
            }
            switchPlayer();
        }
        inputContinue();
    }

    private static void printWelcome() {
        System.out.println("Welcome to xo game!");
    }

    private static void printTable() {
        System.out.println("_____________");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("_____________");
        }
    }

    private static void printTurn() {
        System.out.print(currentPlayer + " turn. ");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row[1-3] and column[1-3]: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                break;

            } else {
                System.out.println(" Please Input Agian!!.");
            }
        }
    }

    private static void switchPlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printWin() {
        System.out.println("Congratulations! " + " Player " + currentPlayer + " Wins! ");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("!!! Draw !!!");
    }

    private static void printAgain() {
        System.out.print("Do you want to play again? (y/n) : ");

    }

    private static void inputContinue() {
        Scanner sc = new Scanner(System.in);
        String newGame = sc.next();
        if (newGame.equals("y")) {
            resetTable();
            main(null);

        } else {
            System.out.println("------ Good Bye ------");
            System.exit(0);
        }
    }

    private static void resetTable() {
       table = new char[][] {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
       currentPlayer = 'X';
    }
}
