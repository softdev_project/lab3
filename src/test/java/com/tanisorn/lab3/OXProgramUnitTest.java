/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.tanisorn.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author uSeR
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1BY_O_output_true() {
        char[][] table = {{'O', 'O', 'O'}, {'X', 'X', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2BY_O_output_true() {
        char[][] table = {{'-', 'X', '-'}, {'O', 'O', 'O'}, {'-', 'X', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3BY_O_output_true() {
        char[][] table = {{'-', 'X', '-'}, {'X', '-', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1BY_X_output_true() {
        char[][] table = {{'X', 'X', 'X'}, {'O', '-', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2BY_X_output_true() {
        char[][] table = {{'-', 'O', 'O'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3BY_X_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'O', '-', '-'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1BY_O_output_true() {
        char[][] table = {{'O', 'X', 'X'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2BY_O_output_true() {
        char[][] table = {{'-', 'O', 'X'}, {'X', 'O', '-'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3BY_O_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'-', 'X', 'O'}, {'-', 'X', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1BY_X_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'X', '-', 'O'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2BY_X_output_true() {
        char[][] table = {{'O', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3BY_X_output_true() {
        char[][] table = {{'-', '-', 'X'}, {'O', 'O', 'X'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinX1BY_O_output_true() {
        char[][] table = {{'O', 'X', 'X'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinX2BY_O_output_true() {
        char[][] table = {{'-', 'X', 'O'}, {'-', 'O', '-'}, {'O', '-', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinX1BY_X_output_true() {
        char[][] table = {{'X', '-', '-'}, {'O', 'X', 'O'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinX2BY_X_output_true() {
        char[][] table = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', 'O', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDraw_output_true() {
        char[][] table = {{'X', 'O', 'O'}, {'O', 'X', 'X'}, {'X', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkDraw(table, currentPlayer));
    }
}
